# Project1

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 13.3.0.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The application will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via a platform of your choice. To use this command, you need to first add a package that implements end-to-end testing capabilities.

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.io/cli) page.


## ############################## containerization manual ##############################
## Creamos la imagen basado en el dockerfile
podman build -f Dockerfile_Dev -t webapp:dev .
podman build --build-arg APP_NAME=project1 --build-arg APP_ENV=prod -f Dockerfile -t webapp:prod .

## Corremos el contenedor
podman run --name webapp_dev -d -p 8080:80 webapp:dev
podman run --name webapp_prod -d -p 8081:80 webapp:prod

## ############################## containerization automatica ##############################
## Recordar que APP_NAME usado es el nombre de la carpeta que se encuentra dentro de dist
## la carpeta dist se genera despues de usar el: npm run build

## Corremos el contenedor basado en el dockerfile
podman-compose -f docker-compose_Prod.yml up -d
podman-compose -f docker-compose_Cert.yml up -d
podman-compose -f docker-compose_Test.yml up -d
podman-compose -f docker-compose_Dev.yml up